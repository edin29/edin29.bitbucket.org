/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;
var obmocje;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function() {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [46.051411, 14.506025],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
  
  // Ročno dodamo fakulteto za računalništvo in informatiko na mapo 

  // Objekt oblačka markerja
  var popup = L.popup();

  //izpisiPodatke();
  narisiMarkerje();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);

    prikazPoti(latlng);
  }
  mapa.on('click', obKlikuNaMapo);
});


/**
 * Za podano vrsto interesne točke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 * 
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function() {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
      var json = JSON.parse(xobj.responseText);

      // vrnemo rezultat
      callback(json.features);
    }
  };
  xobj.send(null);
}

function narisiMarkerje() {
  pridobiPodatke(function(jsonRezultat) {
    for (var i = 0; i < jsonRezultat.length; i++) {
      if (jsonRezultat[i].geometry.type == "Point") {
        var lat = jsonRezultat[i].geometry.coordinates[0];
        var lng = jsonRezultat[i].geometry.coordinates[1];
        
        // obrnemo koordinati
        dodajMarker(lng, lat, jsonRezultat[i]);
      }
    }
  });
}

function dodajMarker(lat, lng, jsonRezultat) {
  var ikona = new L.Icon({
    iconUrl: 'http://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], { icon: ikona });

  // Izpišemo želeno sporočilo v oblaček
  var name = jsonRezultat.properties.name;
  var street = jsonRezultat.properties['addr:street'];
  var housenumber = jsonRezultat.properties['addr:housenumber'];
  var postcode = jsonRezultat.properties['addr:postcode'];
  var city = jsonRezultat.properties['addr:city'];
  var amenity = jsonRezultat.properties.amenity;
  var building = jsonRezultat.properties.building;

  marker.bindPopup((name != null ? name + '<br>' : '') +
    (street != null ? street + ' ' : '') +
    (housenumber != null ? (street != null ? housenumber : '') : '') +
    (postcode != null ? (street != null ? ', ' : '') + postcode + ' ' : '') +
    (city != null ? (postcode != null ? '' : ', ') + city : '') +
    (((street != null) || (housenumber != null) || (postcode != null) || (city != null) != null) ? '<br>' : '') +
    (building != null ? 'Zgradba' : '') +
    (amenity != null ? (building != null ? ': ' : '') + 'bolnisnica' : '')
  );
  
  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}
